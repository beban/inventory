<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Products>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */

    public function definition()
    {

        return [
            'name' => $this->faker->words(2, true),
            'category_id' => $this->faker->numberBetween($min = 1, $max = 6),
            'stock' => $this->faker->numberBetween($min = 1, $max = 1600),
            'price' => $this->faker->numberBetween($min = 1, $max = 200),
        ];
    }
}
