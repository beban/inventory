<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;

use App\Models\User;
use App\Models\Product;
use App\Models\Category;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // account
        User::create([
            'name' => 'admin',
            'username' => 'admin',
            'email' => 'superuser@gmail.com',
            'password' => bcrypt('password')
        ]);

        // products
        Product::factory()->count(120)->create();

        // categories
        Category::create([
            'name' => 'Minuman',
            'slug' => 'minuman'
        ]);

        Category::create([
            'name' => 'Makanan',
            'slug' => 'makanan'
        ]);

        Category::create([
            'name' => 'Kecantikan',
            'slug' => 'kecantikan'
        ]);

        Category::create([
            'name' => 'Tiket & Voucher',
            'slug' => 'tiket-voucher'
        ]);

        Category::create([
            'name' => 'Pakaian',
            'slug' => 'pakaian'
        ]);

        Category::create([
            'name' => 'Elektronik',
            'slug' => 'elektronik'
        ]);
    }
}
